function invert(obj) {
    let invertObject = {};
    for (let key in obj) {
        let temp = key;
        key = obj[key];
        invertObject[key] = temp;
    };
    return invertObject;
};

module.exports = invert;