function mapObject(obj, callback) {
    let newObject = {};
    for (let key in obj) {
        let newValue = callback(obj[key]);
        newObject[key] = newValue;
    };
    return newObject;
};

function copyEachElement(value) {
    return value + `-${value}`;
};

module.exports.mapObject = mapObject;
module.exports.copyEachElement = copyEachElement;