function pairs(obj) {
    let outputArray = [];
    for (let key in obj) {
        let pair = [];
        pair.push(key, obj[key]);
        outputArray.push(pair);
    };
    return outputArray;
};

module.exports = pairs;
