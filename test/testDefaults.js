const defaults = require('../defaults.js');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const defaultProps = { father: "Thomas Wayne"};
const expectedOutput = { name: 'Bruce Wayne', age: 36, location: 'Gotham',father: 'Thomas Wayne' };

const actualOutput = defaults(testObject, defaultProps);

let answerIsCorrect = true;

for (let key in actualOutput) {
    if (actualOutput[key] !== expectedOutput[key]) {
        answerIsCorrect = false;
    };
};

if (answerIsCorrect) {
    console.log("Defaults function is working properly");
} else {
    console.log("Check the function again");
};