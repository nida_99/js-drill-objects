const invert = require('../invert.js');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const expectedOutput = { '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' };
const actualOutput = invert(testObject);

let answerIsCorrect = true;
for (let key in actualOutput) {
    if (actualOutput[key] !== expectedOutput[key]) {
        answerIsCorrect = false;
    };
};

if (answerIsCorrect) {
    console.log("Invert function is working properly");
} else {
    console.log("Check the function again");
};
