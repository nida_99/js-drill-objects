const keys = require('../keys.js');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const expectedOutput = ['name','age','location'];

const actualOutput = keys(testObject);
let answerIsCorrect = true;
for (let index = 0 ; index < actualOutput.length ; index++){
    if(actualOutput[index] !== expectedOutput[index]){
        answerIsCorrect = false ;
    };
};
if(answerIsCorrect){
    console.log("keys function is working properly");
}else{
    console.log("Check the function again"); 
}