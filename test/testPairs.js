const pairs = require('../pairs.js');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const expectedOutput = [['name', 'Bruce Wayne'], ['age', 36], ['location', 'Gotham']];
const actualOutput = pairs(testObject);

let answerIsCorrect = true;

if (JSON.stringify(actualOutput) !== JSON.stringify(expectedOutput)) {
    answerIsCorrect = false;
};
if (answerIsCorrect) {
    console.log("Pairs function is working properly");
} else {
    console.log("Check the function again");
};