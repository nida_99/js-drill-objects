const values = require('../values.js');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const expectedOutput = ['Bruce Wayne', 36, 'Gotham'];
const actualOutput = values(testObject);

let answerIsCorrect = true;
for (let index = 0; index < actualOutput.length; index++) {
    if (actualOutput[index] !== expectedOutput[index]) {
        answerIsCorrect = false;
    };
};
if (answerIsCorrect) {
    console.log("Values function is working properly");
} else {
    console.log("Check the function again");
};