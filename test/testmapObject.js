const mapObject = require('../mapObject.js').mapObject;
const copyEachElement = require('../mapObject.js').copyEachElement;

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const expectedOutput = { name: 'Bruce Wayne-Bruce Wayne', age: '36-36', location: 'Gotham-Gotham' };
const actualOutput = mapObject(testObject, copyEachElement);

let answerIsCorrect = true;
for (let key in actualOutput) {
    if (actualOutput[key] !== expectedOutput[key]) {
        answerIsCorrect = false;
    };
};

if (answerIsCorrect) {
    console.log("mapObject function is working properly");
} else {
    console.log("Check the function again");
};